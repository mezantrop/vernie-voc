# Voice Control for Vernie, the robot based on LEGO BOOST Move Hub

<a href="https://www.buymeacoffee.com/mezantrop" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>

### vernie_voc.py - based on the Snowboy hotword detection system and  pylgbst - the library to interact with LEGO Move Hub

[![Vernie Voice Control in action](https://img.youtube.com/vi/On1otx0HenM/0.jpg)](https://www.youtube.com/watch?v=On1otx0HenM)


## Requirements
* A computer with a Bluetooth adapter
* Linux (Ubuntu 18.10 Desktop works fine for me)
* Python 3
* Snowboy library - https://github.com/Kitt-AI/snowboy
* Pylgbst library - https://github.com/undera/pylgbst
* Bluetooth Pylgbst compatible backend library. The pygatt (https://github.com/peplin/pygatt) module works fine for me

## Installation
Sorry guys, I didn't write an installer yet. But you can try my draft notes below:

1. Install Bluetooth library and amazing pylgbst:

    ```
    $ pip3 install pygatt
    $ pip3 install https://github.com/undera/pylgbst/archive/0.9.tar.gz
    ```
2. Precompiled "Snowboy Binaries with Python Demo" did not work for me as they seem to be prepared for Python 2, 
so I had to install something here and tinker there. Actually, I'm not sure if I can write down all the steps right 
by memory. Anyway, download and unzip Vernie VoC:
    ```
    $ cd /some/path
    $ wget https://gitlab.com/mezantrop/vernie-voc/-/archive/master/vernie-voc-master.zip
    $ unzip vernie-voc-master.zip
    ```

3. Install audio libraries and prepare to compile Snowboy:
    ```
    $ sudo apt install python-pyaudio python3-pyaudio sox
    $ pip3 install pyaudio 
    $ pip3 install https://github.com/Kitt-AI/snowboy/archive/master.zip
    $ unzip master.zip
    ```

4. I have uploaded the directory with precompiled Snowboy libraries for my Ubuntu and Python 3 but you might need to 
compile and patch Snowboy for your environment:
    ```
    $ cd snowboy-master/swig/Python3 && make
    $ cp _snowboydetect.so /some/path/to/vernie-voc-master && cd ../../ 
    $ cp build/lib/snowboy/*.py /some/path/to/vernie-voc-master
    $ sed -i -- 's/from . import snowboydetect/import snowboydetect/g' /some/path/to/vernie-voc-master/snowboydecoder.py
    ```

    In any case, you should have these files to run Vernie VoC:
    ```
    resources/*
    snowboydecoder.py
    snowboydetect.py
    _snowboydetect.so
    vernie_voc.py
    ```

## Usage
1. Turn the robot on by pressing the button on his hull
2. start the voice control:
    ```
    $ sudo -H python3 vernie_voc.py
    ```
3. Wait for initialization. Don't be scared of warnings.
4. You can greet Vernie saying `Ahoj`, it means "_Hi_" in Czech. Command Vernie to `go` forward, `run`, `back`, `left` 
and `right`. Vernie should detect barriers, but you always can shout out `stop`.
5. Have fun! Press `Ctrl-C` to stop the script when you get bored.

_Don't hesitate to contact me: Mikhail Zakharov <zmey20000@yahoo.com>_
