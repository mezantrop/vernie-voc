#!/usr/bin/env python3

# vernie_voc.py - Vernie Voice Control
# Voice Control for Vernie, the robot based on LEGO BOOST Move Hub

# -----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# zmey20000@yahoo.com wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return Mikhail Zakharov
# -----------------------------------------------------------------------------

# snowboy and its materials are licensed under Apache License
# https://github.com/Kitt-AI/snowboy/blob/master/LICENSE

# pylgbst is licensed under MIT License
# https://github.com/undera/pylgbst/blob/master/LICENSE

# 2018.12.18    v0.1    Mikhail Zakharov <zmey20000@yahoo.com>
#   Initial release
# 2018.12.19    v0.2    Mikhail Zakharov <zmey20000@yahoo.com>
#   Licenses, comments, syntax


import sys
import snowboydecoder
from pylgbst.movehub import MoveHub
from pylgbst.movehub import ColorDistanceSensor
from pylgbst import get_connection_auto


STOP_DISTANCE = 5                           # Distance in inches
DIRECTION_FORWARD = 1                       # Directions
DIRECTION_BACKWARD = -1

SPEED_WALK = 0.5                            # Speed definitions
SPEED_RUN = 1


snow_models = [
    'resources/ahoj.pmdl', 'resources/back.pmdl', 'resources/go.pmdl',
    'resources/left.pmdl', 'resources/right.pmdl', 'resources/run.pmdl',
    'resources/stop.pmdl'
]

snow_callbacks = [
    lambda: init_head(), lambda: go_backward(), lambda: go_forward(), 
    lambda: turn_left(), lambda: turn_right(), lambda: run_forward(),
    lambda: vernie_stop()
]


# -----------------------------------------------------------------------------
def run_forward():
    vernie.motor_AB.constant(SPEED_RUN * DIRECTION_FORWARD,
                             SPEED_RUN * DIRECTION_FORWARD)


def go_forward():
    vernie.motor_AB.constant(SPEED_WALK * DIRECTION_FORWARD,
                             SPEED_WALK * DIRECTION_FORWARD)


def go_backward():
    vernie.motor_AB.constant(SPEED_WALK * DIRECTION_BACKWARD,
                             SPEED_WALK * DIRECTION_BACKWARD)


def turn_right():
    vernie.motor_AB.angled(90, SPEED_WALK * DIRECTION_FORWARD,
                           SPEED_WALK * DIRECTION_BACKWARD)


def turn_left():
    vernie.motor_AB.angled(90, SPEED_WALK * DIRECTION_BACKWARD,
                           SPEED_WALK * DIRECTION_FORWARD)


def look_left():
    vernie.motor_external.angled(45, -0.2)


def look_right():
    vernie.motor_external.angled(45, 0.2)


def init_head():
    """Just shake the head"""

    vernie.motor_external.timed(0.5, -0.2)
    look_right()
    look_right()
    look_left()


def vernie_stop():
    """Stop motors"""

    vernie.motor_AB.constant(0, 0)


def check_distance(clr, distance):
    """Check distance and stop if barrier is too close"""

    if distance <= STOP_DISTANCE:
        vernie_stop()


def vernie_exit(pressed):
    """Exit on the button pressed """

    if pressed:
        blc.disconnect()
        sys.exit(0)


# -----------------------------------------------------------------------------
blc = get_connection_auto()
vernie = MoveHub(blc)

vernie.button.subscribe(vernie_exit)
vernie.color_distance_sensor.subscribe(check_distance,
                                       mode=ColorDistanceSensor.COLOR_DISTANCE_FLOAT)

sensitivity = [0.5] * len(snow_models)
snow_detector = snowboydecoder.HotwordDetector(snow_models,
                                               sensitivity=sensitivity)

snow_callbacks = [
    lambda: init_head(), lambda: go_backward(), lambda: go_forward(), 
    lambda: turn_left(), lambda: turn_right(), lambda: run_forward(),
    lambda: vernie_stop()
]

init_head()
snow_detector.start(detected_callback=snow_callbacks, sleep_time=0.05)
